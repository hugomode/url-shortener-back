SHELL=/bin/sh
GIT_DIR=$(shell pwd)
PACKAGE_NAME=url-shortener-back
NETWORK_DOCKER=host
REGISTRY=nexus3.uchile.cl:8085/meli/
VERSION=latest


start-mongo:	
	docker run --name some-mongo --net $(NETWORK_DOCKER) -d mongo

stop-mongo:	
	docker rm some-mongo -f		

restart-mongo: stop-mongo start-mongo	

start-redis:
#	docker run --name some-redis --net $(NETWORK_DOCKER) -d redis
	docker run --name some-redis --net $(NETWORK_DOCKER) -d redis redis-server --save 60 1 --loglevel warning

stop-redis:	
	docker rm some-redis -f	

restart-redis: stop-redis start-redis

start-nginx:
	docker run --name my-custom-nginx-container --net $(NETWORK_DOCKER) -v $(GIT_DIR)/nginx/nginx.conf:/etc/nginx/nginx.conf:ro -d nginx

stop-nginx:	
	docker rm my-custom-nginx-container	-f

restart-nginx: stop-nginx start-nginx


docker-build:
	docker build -t $(PACKAGE_NAME)-dev -f Dockerfile.dev  $(GIT_DIR)

docker-shell:
	docker run -it --rm --env-file=.env -v $(GIT_DIR):/app --net $(NETWORK_DOCKER) -w /app/ --entrypoint=/bin/sh $(PACKAGE_NAME)-dev	

docker-run:
	docker rm $(PACKAGE_NAME)	-f
	docker build -t $(PACKAGE_NAME) -f Dockerfile  $(GIT_DIR)
	docker run --name $(PACKAGE_NAME) --env-file=.env --net $(NETWORK_DOCKER) -d $(PACKAGE_NAME)

docker-build-release:
	docker build -t $(REGISTRY)$(PACKAGE_NAME):$(VERSION) -f Dockerfile  $(GIT_DIR)

docker-push: docker-build-release
	docker image push $(REGISTRY)$(PACKAGE_NAME):$(VERSION)	

go:
	go run cmd/main.go	

json-swagger:
	swag init -g cmd/main.go --ot "json"

deploy-api:	
	helm --namespace=default upgrade -i meli-$(PACKAGE_NAME) --debug -f deploy/values.yaml deploy --kube-context desa07

deploy-mongo:
	helm upgrade -i meli bitnami/mongodb --set serviceAccount.create=false,persistence.enabled=false,auth.enabled=false  --namespace=default --debug  --kube-context desa07

uninstall-meli:
	helm uninstall meli --namespace=default --debug  --kube-context desa07
	helm uninstall meli-$(PACKAGE_NAME) --namespace=default --debug  --kube-context desa07