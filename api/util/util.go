package util

import (
	"crypto/sha256"
	"errors"
	"fmt"
	"math"
	"math/big"
	"strings"

	"github.com/itchyny/base58-go"
	uuid "github.com/nu7hatch/gouuid"
)

const (
	alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	length   = uint64(len(alphabet))
)

func Encode(number uint64) string {
	var encodedBuilder strings.Builder
	encodedBuilder.Grow(11)

	for ; number > 0; number = number / length {
		encodedBuilder.WriteByte(alphabet[(number % length)])
	}

	return encodedBuilder.String()
}

func Decode(encoded string) (uint64, error) {
	var number uint64

	for i, symbol := range encoded {
		alphabeticPosition := strings.IndexRune(alphabet, symbol)

		if alphabeticPosition == -1 {
			return uint64(alphabeticPosition), errors.New("invalid character: " + string(symbol))
		}
		number += uint64(alphabeticPosition) * uint64(math.Pow(float64(length), float64(i)))
	}

	return number, nil
}

func sha256Of(input string) []byte {
	algorithm := sha256.New()
	algorithm.Write([]byte(input))

	return algorithm.Sum(nil)
}

func base58Encoded(bytes []byte) string {
	encoding := base58.BitcoinEncoding

	encoded, err := encoding.Encode(bytes)
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}

	return string(encoded)
}

func GenerateShortLink(link string) string {
	u, err := uuid.NewV4()
	if err != nil {
		panic(err)
	}
	//fmt.Println("uuid: ", u.String())
	urlHashBytes := sha256Of(link + u.String())
	generatedNumber := new(big.Int).SetBytes([]byte(urlHashBytes)).Uint64()
	//fmt.Println("generatedNumber: ", string([]byte(fmt.Sprintf("%d", generatedNumber))))
	finalString := base58Encoded([]byte(fmt.Sprintf("%d", generatedNumber)))
	//fmt.Println("finalString: ", finalString)

	return finalString[:8]
}
