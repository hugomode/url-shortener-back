package model

type Info struct {
	Id      string `json:"_id,omitempty" bson:"_id,omitempty"`
	LongUrl string `json:"long_url"`
	Hit     int    `json:"hit"`
}
