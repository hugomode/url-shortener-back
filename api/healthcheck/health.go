package healthcheck

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/hugomode/url-shortener-back/api/db"
)

func HealthCheck(c *gin.Context) {
	if err := db.Ping(c.Request.Context()); err != nil {
		c.JSON(http.StatusServiceUnavailable, gin.H{"status": "unhealty"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"status": "pong"})
}
