package api

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"os"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"

	"gitlab.com/hugomode/url-shortener-back/api/db"
	"gitlab.com/hugomode/url-shortener-back/api/model"
	"gitlab.com/hugomode/url-shortener-back/api/util"
)

// Crear URL corta a partir de una URL larga
// @Summary      Genera una URL corta que es más amigable con el usuario final
// @Accept       json
// @Produce      json
// @Param		 longUrl body string true "url larga" SchemaExample("https://eletronicos.mercadolivre.com.br/seguranca-casa/#menu=categories")
// @Success      200 {object} string
// @Failure      400 {object} string
// @Failure      404 {object} string
// @Failure      500 {object} string
// @Router       /encode [post]
func EncodeLink(c *gin.Context) {
	var longUrl string
	var err error
	var statusCode int
	defer func() {
		if err != nil {
			log.Errorf("<<<ERROR: %v", err)
			c.JSON(statusCode, gin.H{
				"success": false,
				"error":   err.Error(),
			})
		}
	}()
	if err = c.BindJSON(&longUrl); err != nil {
		statusCode = http.StatusBadRequest
		return
	}
	var urlParse *url.URL
	if urlParse, err = url.ParseRequestURI(longUrl); err != nil {
		statusCode = http.StatusBadRequest
		return
	}
	shortLink := util.GenerateShortLink(urlParse.String())
	data := model.Info{LongUrl: longUrl, Hit: 0}

	if err = db.Set(shortLink, data); err != nil {
		statusCode = http.StatusInternalServerError
		return
	}
	var shortHost *url.URL
	if shortHost, err = url.ParseRequestURI(os.Getenv("SHORT_HOST")); err != nil {
		statusCode = http.StatusInternalServerError
		fmt.Println("env: ", os.Getenv("SHORT_HOST"))
		return
	}
	c.JSON(http.StatusOK, gin.H{"success": true, "short_url": fmt.Sprintf("%s/%s", shortHost.String(), shortLink)})

}

// Redireccionar a la URL original o larga
// @Summary      A partir de una URL corta redirecciona a la URL original
// @Accept       json
// @Produce      json
// @Param		 code path string true "código que representa a la URL larga codificada"
// @Success      301 {object} string
// @Failure      400 {object} string
// @Failure      404 {object} string
// @Failure      500 {object} string
// @Router       /{code} [get]
func RedirectLongLink(c *gin.Context) {
	var err error
	var statusCode int
	defer func() {
		if err != nil {
			log.Errorf("<<<ERROR: %v", err)
			c.JSON(statusCode, gin.H{
				"success": false,
				"error":   err.Error(),
			})
		}
	}()
	code := c.Param("code")

	info, err := db.Get(code)
	if err != nil {
		statusCode = http.StatusInternalServerError
		return
	}
	if info == nil {
		err = errors.New(fmt.Sprintf("not found key to redirect: %s", code))
		statusCode = http.StatusNotFound
		return
	}
	info.Hit++
	if err = db.Set(code, *info); err != nil {
		statusCode = http.StatusInternalServerError
		return
	}
	c.Redirect(http.StatusMovedPermanently, info.LongUrl)
}

// Eliminar URL registrada
// @Summary      Para eliminar una URL registrada en la BD
// @Accept       json
// @Produce      json
// @Param		 code path string true "código que representa a la URL larga codificada"
// @Success      204
// @Failure      400 {object} string
// @Failure      404 {object} string
// @Failure      500 {object} string
// @Router       /{code} [delete]
func DeleteLink(c *gin.Context) {
	var err error
	var statusCode int
	defer func() {
		if err != nil {
			log.Errorf("<<<ERROR: %v", err)
			c.JSON(statusCode, gin.H{
				"success": false,
				"error":   err.Error(),
			})
		}
	}()
	code := c.Param("code")
	var isDelete bool
	if isDelete, err = db.Delete(code); err != nil {
		statusCode = http.StatusInternalServerError
		return
	}
	if !isDelete {
		err = errors.New(fmt.Sprintf("not found key to delete: %s", code))
		statusCode = http.StatusNotFound
		return
	}
	c.JSON(http.StatusNoContent, nil)
}

// Obtener objeto registrado en la BD
// @Summary      Para obtener el objeto registrado en la BD destacando el contador que indica cuántas veces se redireccionó a la URL original
// @Accept       json
// @Produce      json
// @Param		 code path string true "código que representa a la URL larga codificada"
// @Success      200 {object} model.Info
// @Failure      400 {object} string
// @Failure      404 {object} string
// @Failure      500 {object} string
// @Router       /{code}/stats [get]
func Stats(c *gin.Context) {
	var err error
	var statusCode int
	defer func() {
		if err != nil {
			log.Errorf("<<<ERROR: %v", err)
			c.JSON(statusCode, gin.H{
				"success": false,
				"error":   err.Error(),
			})
		}
	}()
	code := c.Param("code")

	info, err := db.Get(code)
	if err != nil {
		statusCode = http.StatusInternalServerError
		return
	}
	if info == nil {
		err = errors.New(fmt.Sprintf("not found key: %s", code))
		statusCode = http.StatusNotFound
		return
	}
	c.JSON(http.StatusOK, info)
}
