package db

import (
	"context"
	"errors"

	"gitlab.com/hugomode/url-shortener-back/api/model"
)

const (
	DB_FILE  = "files"
	DB_REDIS = "redis"
	DB_MONGO = "mongo"
	ACTIVE   = DB_MONGO
)

func Setup() error {
	switch ACTIVE {
	case DB_FILE:
		return SetupFile()
	case DB_REDIS:
		return SetupRedis()
	case DB_MONGO:
		return SetupMongo()
	default:
		return errors.New("not implemented!")
	}
}

func Set(key string, value interface{}) error {
	switch ACTIVE {
	case DB_FILE:
		return SetFile(key, value)
	case DB_REDIS:
		return SetRedis(key, value)
	case DB_MONGO:
		return SetMongo(key, value)
	default:
		return errors.New("not implemented!")
	}
}

func Get(key string) (*model.Info, error) {
	switch ACTIVE {
	case DB_FILE:
		return nil, errors.New("not implemented!")
	case DB_REDIS:
		return GetRedis(key)
	case DB_MONGO:
		return GetMongo(key)
	default:
		return nil, errors.New("not implemented!")
	}
}

func Delete(key string) (bool, error) {
	switch ACTIVE {
	case DB_FILE:
		return false, errors.New("not implemented!")
	case DB_REDIS:
		return DeleteRedis(key)
	case DB_MONGO:
		return DeleteMongo(key)
	default:
		return false, errors.New("not implemented!")
	}
}

func Ping(c context.Context) error {
	switch ACTIVE {
	case DB_FILE:
		return errors.New("not implemented!")
	case DB_REDIS:
		return errors.New("not implemented!")
	case DB_MONGO:
		return PingMongo(c)
	default:
		return errors.New("not implemented!")
	}
}
