package db

import (
	"context"
	"fmt"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/hugomode/url-shortener-back/api/model"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
)

var collection *mongo.Collection
var client *mongo.Client

func SetupMongo() error {
	host := os.Getenv("BD_HOST")
	port := os.Getenv("BD_PORT")
	clientOpts := options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%s", host, port))
	ctx := context.TODO()
	var err error
	client, err = mongo.Connect(ctx, clientOpts)

	if err != nil {
		log.Fatal(err)
	}

	// Check the connections
	err = client.Ping(ctx, nil)
	if err != nil {
		return err
	}
	collection = client.Database(os.Getenv("DB_NAME")).Collection(os.Getenv("BD_COLLECTION"))
	log.Info("Congratulations, you're already connected to MongoDB!")
	return nil
}

func PingMongo(c context.Context) error {
	ctx, cancel := context.WithTimeout(c, 30*time.Second)
	defer cancel()

	// Check the connections
	err := client.Ping(ctx, nil)
	if err != nil {
		return err
	}
	return nil
}

// Set a key/value
func SetMongo(key string, value interface{}) error {
	info := value.(model.Info)
	info.Id = key
	update := bson.M{"$set": info}
	_, err := collection.UpdateOne(context.TODO(), bson.M{"_id": key},
		update, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}

	return nil
}

// Get get a key
func GetMongo(key string) (*model.Info, error) {
	var result model.Info
	err := collection.FindOne(context.TODO(), bson.M{"_id": key}).Decode(&result)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, nil
		}
		return nil, err
	}
	return &result, nil
}

// Delete delete a kye
func DeleteMongo(key string) (bool, error) {
	result, err := collection.DeleteOne(context.TODO(), bson.M{"_id": key})
	if err != nil {
		return false, err
	}
	return result.DeletedCount > 0, nil
}
