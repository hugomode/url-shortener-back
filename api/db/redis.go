package db

import (
	"encoding/json"
	"fmt"

	"os"
	"time"

	"github.com/gomodule/redigo/redis"
	"gitlab.com/hugomode/url-shortener-back/api/model"
)

var redisConn *redis.Pool

// Setup Initialize the Redis instance
func SetupRedis() error {
	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")
	redisConn = &redis.Pool{
		MaxIdle:     30,
		MaxActive:   30,
		IdleTimeout: 30,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", fmt.Sprintf("%s:%s", host, port))
			if err != nil {
				return nil, err
			}
			if os.Getenv("DB_PASSWORD") != "" {
				if _, err := c.Do("AUTH", os.Getenv("DB_PASSWORD")); err != nil {
					c.Close()
					return nil, err
				}
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
	return nil
}

// Set a key/value
func SetRedis(key string, value interface{}) error {
	//n := time.Now()
	conn := redisConn.Get()
	defer func() {
		conn.Close()
		//fmt.Println("duration ms: ", time.Since(n).Milliseconds())
	}()

	b, err := json.Marshal(value)
	if err != nil {
		return err
	}
	_, err = conn.Do("SET", key, b)
	if err != nil {
		return err
	}
	return nil
}

// Get get a key
func GetRedis(key string) (*model.Info, error) {
	conn := redisConn.Get()
	defer conn.Close()

	reply, err := redis.Bytes(conn.Do("GET", key))
	if err != nil {
		return nil, err
	}
	result := model.Info{}
	err = json.Unmarshal(reply, &result)
	return &result, err
}

// Exists check a key
func ExistsRedis(key string) bool {
	conn := redisConn.Get()
	defer conn.Close()

	exists, err := redis.Bool(conn.Do("EXISTS", key))
	if err != nil {
		return false
	}

	return exists
}

// Delete delete a kye
func DeleteRedis(key string) (bool, error) {
	conn := redisConn.Get()
	defer conn.Close()

	return redis.Bool(conn.Do("DEL", key))
}
