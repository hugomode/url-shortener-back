package db

import (
	"encoding/json"
	"fmt"
	"os"
)

func SetupFile() error {
	return nil
}

// Set a key/value
func SetFile(key string, value interface{}) error {

	file, err := os.Create(fmt.Sprintf("%s/%s.json", "files/", key))
	defer func() {
		file.Close()
	}()

	if err != nil {
		return err
	}
	b, err := json.Marshal(value)
	if err != nil {
		return err
	}

	_, err = file.Write(b)
	if err != nil {
		return err
	}

	return nil
}
