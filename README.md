# url-shortener-back

Proyecto que se crea para mi postulación de empleo para mercado libre

## Requisitos para ejecutar este proyecto

- [docker](https://docs.docker.com/engine/install/ubuntu/)
- Opcionalmente [Make](https://www.gnu.org/software/make/) para simplificar la ejecución en tu local

## Requisitos para ejecutar este proyecto localmente

1.- Este proyecto usa como BD [mongoDB](https://www.mongodb.com/), para eso se debes levantar imagen docker con esta BD con esta instrucción

```bash
make start-mongo
```

ó para eliminar imagen docker de la BD y volver a levantar:

```bash
make restart-mongo
```

2. Para levantar este proyecto en una imagen docker

```bash
make docker-run
```

## Requisitos para levantar este proyecto localmente en modo dev

1. Levantar BD mongoDB como se indica [anteriormente](https://gitlab.com/hugomode/url-shortener-back/-/blob/main/README.md#requisitos-para-ejecutar-este-proyecto-localmente)

2. Para construir imagen docker para dev

```bash
make docker-build
```

3. Ejecutar imagen docker y entrar en modo sh

```bash
make docker-shell
```

4. Ejecutar proyecto dentro de la imagen docker

```bash
make go
```

## CURLs de ejemplo para los endpoints disponibles

Para visualizar el swagger del proyecto, copiar el contenido de este [json](https://gitlab.com/hugomode/url-shortener-back/-/blob/main/docs/swagger.json) y pegarlo en este sitio https://editor.swagger.io/

1. Create short url

```
curl -X POST http://localhost:8080/encode -H "Content-Type: application/json" -d '"https://eletronicos.mercadolivre.com.br/seguranca-casa/#menu=categories"'
```

2. redirect long url (replace "xyz" by short url)

```
curl -X GET http://localhost:8080/xyz -H "Content-Type: application/json"
```

3. delete url (replace "xyz" by short url)

```
curl -X DELETE http://localhost:8080/xyz -H "Content-Type: application/json"
```

4. statistics short url (replace "xyz" by short url)

```
curl -X GET http://local.uchile.cl:8080/xyz/stats -H "Content-Type: application/json"
```

