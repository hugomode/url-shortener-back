package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.com/hugomode/url-shortener-back/api"
	"gitlab.com/hugomode/url-shortener-back/api/db"
	"gitlab.com/hugomode/url-shortener-back/api/healthcheck"
)

// @title 		   URL SHORTENER BACK
// @version        1.0.0
// @contact.name   Hugo Cárcamo
// @contact.url    http://www.swagger.io/support
// @contact.email  hugomode@gmail.com

// @license.name  Apache 2.0
// @license.url   http://www.apache.org/licenses/LICENSE-2.0.html
// @host      	  localhost:8080
// @schemes http https

func main() {

	r := gin.Default()
	r.POST("/encode", api.EncodeLink)
	r.GET("/:code", api.RedirectLongLink)
	r.DELETE("/:code", api.DeleteLink)
	r.GET("/:code/stats", api.Stats)
	r.GET("/health", healthcheck.HealthCheck)
	err := db.Setup()
	if err != nil {
		log.Fatal(fmt.Sprintf("<<<ERROR No se establece conexión a BD %v", err))
	}
	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
